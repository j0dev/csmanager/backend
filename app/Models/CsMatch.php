<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * CS:GO Match object
 */
class CsMatch extends BaseModel
{
  use SoftDeletes;

  protected $casts = [
    "maplist" => "array",
    "cvars" => "array",
    "extra" => "array",
    "skip_veto" => "boolean",
    "upload_demo" => "boolean",
  ];

  public function team1()
  {
    return $this->belongsTo(CsTeam::class, "team1_id");
  }

  public function team2()
  {
    return $this->belongsTo(CsTeam::class, "team2_id");
  }

  public function spectators()
  {
    return $this->belongsTo(CsTeam::class, "spectator_id");
  }

  public function server()
  {
    return $this->belongsTo(CsServer::class);
  }

  public function winner()
  {
    return $this->belongsTo(CsTeam::class, "winner_id");
  }

  /**
   * isEditable returns if the model can still be edited by a user
   * @return bool
   */
  public function isEditable()
  {
    if ($this->state == "created" || $this->state == "scheduled") return true;
    return false;
  }

  /**
   * isDeletable returns if the model can be deleted by a user
   * @return bool
   */
  public function isDeletable()
  {
    if ($this->state == "loading" || $this->state == "loaded" || $this->state == "running") return false;
    return true;
  }

  /**
   * isCancelable returns if the model can be canceled by a user
   * @return bool
   */
  public function isCancelable()
  {
    if ($this->state == "loaded" || $this->state == "running") return true;
    return false;
  }

  /**
   * isDeployable returns if the match can be deployed
   * @return bool
   */
  public function isDeployable()
  {
    if (!$this->server instanceof Server || $this->server->trashed()) return false;
    if (!$this->team1 instanceof Team || $this->team1->trashed()) return false;
    if (!$this->team2 instanceof Team || $this->team2->trashed()) return false;
    if ($this->state !== "created" && $this->state !== "scheduled") return false;
    if ($this->server->status !== "available") return false;
    return true;
  }

  /**
   * isAutoDeployable returns if the match can be deployed on any available server
   * @return bool
   */
  public function isAutoDeployable()
  {
    if ($this->server instanceof Server) return false;
    if (!$this->team1 instanceof Team || $this->team1->trashed()) return false;
    if (!$this->team2 instanceof Team || $this->team2->trashed()) return false;
    if ($this->state !== "created" && $this->state !== "scheduled") return false;
    return true;
  }

  /**
   * toMatchConfig returns the get5 match config in (json) array format
   * @return array
   */
  public function toMatchConfig()
  {
    $maplist = $this->maplist;
    if ($this->isDeployable()) {
      //$baseUrl = str_replace([request()->path()], '', request()->url());
      $conf = [
        'matchid' => (string) $this->id,
        'num_maps' => $this->num_maps,
        'maplist' => $maplist,
        'skip_veto' => (bool) $this->skip_veto,
        'veto_first' => $this->veto_first,
        'side_type' => $this->side_type,
        'players_per_team' => $this->players_per_team,
        'min_players_to_ready' => 1,

        'match_title' => $this->name,

        'team1' => $this->team1->toMatchConfig(),
        'team2' => $this->team2->toMatchConfig(),

        'cvars' => [
          // 'get5_web_api_url' => $baseUrl,
          'get5_hostname_format' => $this->name.': {TEAM1} vs {TEAM2}',
          'get5_web_upload_demo' => ($this->upload_demo) ? '1' : '0',
        ],
      ];
      if ($this->spectator) {
        $conf['spectators'] = $this->spectator->toSpectatorMatchConfig();
      }
      return $conf;
    }
  }
}
