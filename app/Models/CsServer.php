<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * CS:GO server object
 */
class CsServer extends BaseModel
{
  use SoftDeletes;

  protected $hidden = [
    "rcon_password",
    "api_token",
    "panel_url",
  ];

  protected $casts = [
    "last_updated" => "datetime",
    "version" => "array",
    "allow_demo_upload" => "boolean",
    "public" => "boolean",
  ];

  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
      $model->generateToken();
    });
  }

  protected function generateToken()
  {
    $this->attributes['api_token'] = Str::random(32);
  }

  /**
   * isOnline returns if the server is online or not
   * @return bool
   */
  public function isOnline()
  {
    switch ($this->status) {
      case 'offline':
      case 'testing':
      case 'error':
        return false;
      case 'available':
      case 'inmatch':
      case 'restarting':
        return true;
    }
    return false;
  }
}
