<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * CS:GO Team object
 * Can be a player team and/or spectator group
 */
class CsTeam extends BaseModel
{
  use SoftDeletes;

  protected $casts = [
    "players" => "array",
    "coaches" => "array",
  ];

  public function getMatches()
  {
    $t1 = $this->hasMany(CsMatch::class, 'team1_id')->get();
    $t2 = $this->hasMany(CsMatch::class, 'team2_id')->get();
    $all = $t1->concat($t2);
    $all = $all->sortByDesc('started_at');
    return $all;
  }

  /**
   * playerSteamIds retrieves all the steamid's from the players
   * @return array SteamId's
   */
  public function playerSteamIds()
  {
    $ids = [];

    foreach ($this->players as $player) {
      if (array_key_exists("steamid", $player)) { // if steamid player
        $ids[] = $player["steamid"];
      } elseif(array_key_exists("user", $player)) { // if user player
        // retrieve user
        $user = User::find($player["user"]);
        if ($user) {
          // get linked steamids
          $steamids = $user->steamIds()->get();
          foreach ($steamids as $steamid) {
            // add each steamid64
            $ids[] = $steamid->steamid64;
          }
        }
      }
    }

    return $ids;
  }

  /**
   * playerSteamIds retrieves all the steamid's from the coaches
   * @return array SteamId's
   */
  public function coachSteamIds()
  {
    $ids = [];

    foreach ($this->coaches as $player) {
      if (array_key_exists("steamid", $player)) { // if steamid player
        $ids[] = $player["steamid"];
      } elseif(array_key_exists("user", $player)) { // if user player
        // retrieve user
        $user = User::find($player["user"]);
        if ($user) {
          // get linked steamids
          $steamids = $user->steamIds()->get();
          foreach ($steamids as $steamid) {
            // add each steamid64
            $ids[] = $steamid->steamid64;
          }
        }
      }
    }

    return $ids;
  }

  /**
   * Returns a team array for exporting to a get5 match config
   * @return array get5 match config team section
   */
  public function toMatchConfig()
  {
    return [
      'name' => $this->name,
      'tag' => $this->tag,
      'flag' => $this->c1($this->flag),
      'logo' => $this->c1($this->logo),
      'players' => array_merge($this->playerSteamIds(), $this->coachSteamIds()),
    ];
  }

  /**
   * Returns a team array for exporting to a get5 spectator match config
   * @return array get5 match config spectator section
   */
  public function toSpectatorMatchConfig()
  {
    return [
      'players' => array_merge($this->playerSteamIds(), $this->coachSteamIds()),
    ];
  }

  /**
   * Returns variable or an empty string if null
   * @return string
   */
  private function c1($v)
  {
    return ($v)?$v:"";
  }
}
