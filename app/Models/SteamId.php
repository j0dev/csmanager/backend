<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laratrust\Contracts\Ownable;

class SteamId extends Model implements Ownable
{
  protected $primaryKey = 'steamid64';

  public function getIncrementing()
  {
    return false;
  }

  public function getKeyType()
  {
    return 'string';
  }

  /**
   * Ownership key
   */
  public function ownerKey($owner)
  {
      return $this->user->id;
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
