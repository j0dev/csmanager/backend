<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CsServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_servers', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('name');

            $table->string('address');
            $table->unsignedSmallInteger('game_port')->default(27015);
            $table->unsignedSmallInteger('rcon_port')->default(27015);
            $table->string('rcon_password');

            $table->enum('status', ['offline', 'testing', 'error', 'available', 'inmatch', 'restarting'])->default('offline');

            $table->timestamp('last_update')->nullable();
            $table->text('last_msg')->nullable();

            $table->json('version')->nullable(); // json array to store versions (game, plugins, etc)

            $table->boolean('public')->default(0);

            $table->string('api_token'); // server token to authenticate updates
            $table->string('panel_url'); // url to report back to the server

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_servers');
    }
}
