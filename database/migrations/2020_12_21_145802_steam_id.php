<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SteamId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steam_ids', function (Blueprint $table) {
            $table->string('steamid64', 19)->primary();

            $table->string('steam_name');
            $table->string('avatar');
            $table->string('custom_url')->nullable();

            $table->foreignUuid('user_id')->nullable()->constrained();

            $table->timestamp('last_update')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('steam_ids');
    }
}
