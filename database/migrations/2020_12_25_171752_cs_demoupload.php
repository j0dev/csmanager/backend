<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CsDemoupload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // demo upload setting per server (server admin setting)
        Schema::table('cs_servers', function (Blueprint $table) {
            $table->boolean('allow_demo_upload')->default(1);
        });

        // demo upload setting per match (match admin setting)
        Schema::table('cs_matches', function (Blueprint $table) {
            $table->boolean('upload_demo')->default(1);
        });

        // demo info storage
        Schema::create('cs_match_demos', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('match_id');
            $table->foreign('match_id')->references('id')->on('cs_matches');

            $table->tinyInteger('demo_nr');

            $table->string('filename');
            $table->boolean('uploaded');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_match_demos');
    }
}
