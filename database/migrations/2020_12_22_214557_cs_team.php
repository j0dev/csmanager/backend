<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CsTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_teams', function(Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('name');
            $table->string('tag');

            $table->string('flag')->default("")->nullable();
            $table->string('logo')->default("")->nullable();

            $table->json('players'); // list of steamid's or users
            $table->json('coaches'); // list of steamid's or users

            $table->enum('type', ['normal', 'spectator', 'both'])->default('normal');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_teams');
    }
}
