<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Groups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // group table
        Schema::create('groups', function(Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->text('description');

            $table->enum('visibility', ['public', 'private']);

            $table->uuid('parent_id')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('groups', function(Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('groups');
        });
        // resource pivot table (server / team / match)
        Schema::create('group_resources', function(Blueprint $table) {
            $table->foreignUuid('group_id')->constrained();
            $table->uuidMorphs('resource');
            $table->boolean('readonly')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('group_resources');
        Schema::drop('groups');
    }
}
