<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CsMatchstats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_match_stats', function(Blueprint $table) {
            $table->uuid('id')->primary();

            $table->uuid('match_id');
            $table->foreign('match_id')->references('id')->on('cs_matches');

            $table->tinyInteger('map_nr');
            $table->string('map_name');

            $table->tinyInteger('team1_score')->default(0);
            $table->tinyInteger('team2_score')->default(0);

            $table->tinyInteger('winner_team')->nullable();
            $table->boolean('forfeit')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['match_id', 'map_nr']);
        });

        Schema::create('cs_match_stats_players', function(Blueprint $table) {
          $table->uuid('id')->primary();

          $table->uuid('match_stat_id');
          $table->foreign('match_stat_id')->references('id')->on('cs_match_stats');

          $table->uuid('match_id');
          $table->foreign('match_id')->references('id')->on('cs_matches');

          $table->tinyInteger('map_nr');
          $table->string('player');

          $table->uuid('team_id');
          $table->foreign('team_id')->references('id')->on('cs_teams');

          $table->smallInteger('kills')->nullable();
          $table->smallInteger('assists')->nullable();
          $table->smallInteger('deaths')->nullable();
          $table->smallInteger('flashbang_assists')->nullable();
          $table->smallInteger('teamkills')->nullable();
          $table->smallInteger('suicides')->nullable();
          $table->smallInteger('damage')->nullable(); // decimal???
          $table->smallInteger('headshot_kills')->nullable();
          $table->smallInteger('roundsplayed')->nullable();
          $table->smallInteger('bomb_plants')->nullable();
          $table->smallInteger('bomb_defuses')->nullable();
          $table->smallInteger('1kill_rounds')->nullable();
          $table->smallInteger('2kill_rounds')->nullable();
          $table->smallInteger('3kill_rounds')->nullable();
          $table->smallInteger('4kill_rounds')->nullable();
          $table->smallInteger('5kill_rounds')->nullable();
          $table->smallInteger('v1')->nullable();
          $table->smallInteger('v2')->nullable();
          $table->smallInteger('v3')->nullable();
          $table->smallInteger('v4')->nullable();
          $table->smallInteger('v5')->nullable();
          $table->smallInteger('firstkill_t')->nullable();
          $table->smallInteger('firstkill_ct')->nullable();
          $table->smallInteger('firstdeath_t')->nullable();
          $table->smallInteger('firstdeath_ct')->nullable();
          $table->smallInteger('tradekill')->nullable();
          $table->smallInteger('contribution_score')->nullable();
          $table->smallInteger('kast')->nullable();
          $table->smallInteger('mvp')->nullable();

          $table->timestamps();

          $table->unique(['match_id', 'map_nr', 'player']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_match_stats_players');
        Schema::drop('cs_match_stats');
    }
}
