<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CsMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cs_matches', function(Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('name');
            $table->text('description')->nullable();

            $table->enum('state', ['created', 'scheduled', 'loading', 'loaded', 'running', 'finished', 'cancelled'])->default('created');

            // cant use foreignUuid as we have special column names
            $table->uuid('team1_id')->nullable();
            $table->foreign('team1_id')->references('id')->on('cs_teams');
            $table->uuid('team2_id')->nullable();
            $table->foreign('team2_id')->references('id')->on('cs_teams');

            $table->uuid('spectator_id')->nullable();
            $table->foreign('spectator_id')->references('id')->on('cs_teams');

            $table->unsignedTinyInteger('num_maps')->default(1);
            $table->boolean('skip_veto')->default(0);
            $table->json('maplist');
            $table->enum('veto_first', ['team1', 'team2'])->default('team1');
            $table->enum('side_type', ['standard', 'never_knife', 'always_knife'])->default('standard');
            $table->unsignedTinyInteger('players_per_team')->default(5);
            $table->json('cvars')->default('[]');

            $table->uuid('server_id')->nullable();
            $table->foreign('server_id')->references('id')->on('cs_servers');

            $table->timestamp('started_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->timestamp('scheduled_at')->nullable();

            $table->uuid('winner_id')->nullable();
            $table->foreign('winner_id')->references('id')->on('cs_teams');

            $table->tinyInteger('team1_score')->default(0);
            $table->tinyInteger('team2_score')->default(0);
            $table->string('forfeit')->nullable();

            $table->json("extra");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cs_matches');
    }
}
